import java.io.*;
import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.* ;
import org.apache.jena.rdf.model.*;
import org.apache.jena.util.FileManager;

public class Query1 {

	//Declaration des deux fichiers turtles utilisés pour la requête
	static final String fileCommunesFR = "RDFCommunes.ttl";
	static final String fileCoLA = "RDFCoLA.ttl";
	
    public static void main (String args[]) {
		Model communesFR = ModelFactory.createDefaultModel();
		Model coLA = ModelFactory.createDefaultModel();
		Model model = ModelFactory.createDefaultModel();
        
		InputStream inCommunesFR = FileManager.get().open(fileCommunesFR);
		if (inCommunesFR == null) {
			throw new IllegalArgumentException("File : " + fileCommunesFR + " not found");
		}
		
		InputStream inCoLA = FileManager.get().open(fileCoLA);
		if (inCoLA == null) {
			throw new IllegalArgumentException("File : " + fileCoLA + " not found");
		}

		communesFR.read(inCommunesFR,null, "Turtle");
		coLA.read(inCoLA ,null, "Turtle");
	
	model.add(communesFR);
	model.add(coLA);
		
        String queryString = "PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>\n" + 
        		"PREFIX dbpedia: <http://fr.dbpedia.org/ressource/>\n" + 
				"PREFIX idemo: <http://rdf.insee.fr/def/demo#>\n" + 
				"PREFIX rdag1:  <http://rdvocab.info/Elements/>\n" + 
				"PREFIX sdmx-code:  <http://purl.org/linked-data/sdmx/2009/code#> \n" + 
				"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n" + 
				"SELECT DISTINCT ?commune ?nbHab \n"+
				"WHERE{\n"+
				"{\n"+
				"SELECT DISTINCT ?commune ?nbHab \n"+
        		"WHERE{\n"+
        		"  ?x dbpedia-owl:commune ?commune;\n"+
	    		"     dbpedia:technology ?tech.\n "+
	    		"  ?tech rdag1:transmissionSpeed \"100 Mbit/s et +\";\n"+
				" 		 sdmx-code:Decimals ?value.\n"+
				"  ?y dbpedia-owl:commune ?commune;\n"+
				"  		idemo:population ?pop;\n"+
	    		"  FILTER(xsd:integer(?pop) <= 15000)\n"+
	    		"  BIND(xsd:integer(?pop) as ?nbHab).\n"+
				"}"+
    			"ORDER BY ASC (?nbHab)\n"+
    			"LIMIT 10"+
	    		"}"+
    			"UNION"+
				"{\n"+
				"SELECT DISTINCT ?commune ?nbHab \n"+
        		"WHERE{\n"+
        		"  ?x dbpedia-owl:commune ?commune;\n"+
	    		"     dbpedia:technology ?tech.\n "+
	    		"  ?tech rdag1:transmissionSpeed \"100 Mbit/s et +\";\n"+
				" 		 sdmx-code:Decimals ?value.\n"+
				"  ?y dbpedia-owl:commune ?commune;\n"+
				"  		idemo:population ?pop;\n"+
	    		"  FILTER(xsd:integer(?pop) >= 35000)\n"+
	    		"  BIND(xsd:integer(?pop) as ?nbHab).\n"+
				"}"+
    			"ORDER BY ASC (?nbHab) \n"+
				"LIMIT 10\n"+
	    		"}"+
	    		"}";
    	
    	Query query = QueryFactory.create(queryString) ;
    	
    	
    	try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
    		ResultSet results = qexec.execSelect() ;
    		ResultSetFormatter.out(System.out, results, query) ;
	    }
    }
}