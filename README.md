# Projet de Web Sémantique 2018-2019 

On s'intéresse dans ce projet au découpage des communes en France, à partir de données
de l'INSEE de 2018.  
Les données ont été extraite à partir du site OpenDataSoft (https://data.opendatasoft.com/explore/dataset/admin-express-communes-2018%40public/table/?flg=fr&sort=population)  

Projet réalisé dans le cadre du module "Web des données, web sémantique" du M1(ALMA-ATAL), enseigné par Hala Skaf-Molli à l'UFR Sciences et Techniques de Nantes.

Fichiers csv et ttl trop volumineux pour gitlab, les télécharger ici https://uncloud.univ-nantes.fr/index.php/f/128228970.

## Remarques
Lors de la génération du fichier ttl des erreurs étaient généré à cause du fichier csv, en effet, la présence d'espace dans le nom de certaines villes
créer des problèmes pour la sémantification des données.
Il a donc été fait le choix de remplacer ces espaces par une chaine vide.

Il a été fait le choix de séparer le champ coordonnées fourni par le site OpenDataSoft, en deux champs : longitute et latitude.

Voici le lien des slides : https://docs.google.com/presentation/d/1KuNvbSIrt-U7inxpM99os4Fqb994kKih2vaVmSSv4yU/edit?usp=sharing
